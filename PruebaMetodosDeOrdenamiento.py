'''
Created on 4 dic. 2020

@author: Cesar
'''
class Corrector:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r
class BusquedaBinaria:
    def binario(self, matriz, valorBuscado):
        if(len(matriz)==0):
            return -1;
        mitad = 0
        inferior = 0
        superior = len(matriz)-1
        while(matriz[mitad] != valorBuscado and inferior <= superior):
            mitad = int((inferior+superior)/2)
            if(valorBuscado> matriz[mitad]):
                inferior = mitad +1
            else:
                superior = mitad -1
        if(matriz[mitad]==valorBuscado):
            print("Encontrado")
            return mitad
        else:
            print("No se encontro")
            return -1
class FuncionHash:
    def __init__(self):
        self.table = [None] * 127

    def funcion(self, value):
        clave = 0
        for i in range(0,len(value)):
            clave += ord(value[i])
        return clave % 127

    def Insertar(self, value):
        hash = self.funcion(value)
        if self.table[hash] is None:
            self.table[hash] = value

    def Buscar(self,value):
        hash = self.funcion(value);
        if self.table[hash] is None:
            return None
        else:
            return hex(id(self.table[hash]))

    def Eliminar(self,value):
        hash = self.funcion(value);
        if self.table[hash] is None:
            print(f"El elemento {value} no existe")
        else:
            print(f" el valor {value} a sido eliminado eliminado")
            self.table[hash] is None;
    
    def mostrar(self):
        for i in range(len(self.table)):
            if(self.table[i]!=None):
                print(self.table[i] + ", ", end="")
                
    
            
c = Corrector()  
m = BusquedaBinaria()
f = FuncionHash()
opcion = 0
op = 0
matriz = [1,2,4,6,7,9,13,23,29,34,39,45,48,68,73,81,99,102]
matrizS = ["1","2","4","6","7","9","13","23","29","34","39","45","48","68","73","81","99","102"]
while(opcion!=3):
    print("========== MENU ==========")
    print("Digite 1 para usar le metodo de busqueda binaria")
    print("Digite 2 para usar el metodo de función Hash")
    print("Digite 3 para ***SALIR***")
    opcion = c.correcion()
    if(opcion==1):
        print("========== Metodo de Busqueda Binaria ==========")
        print("Ingrese el valor buscado")
        valor = c.correcion()
        print(f"Vector: {matriz}")
        m.binario(matriz, valor)
    elif(opcion==2):
        print(f"========== Metodo de Funcion Hash ===========")
        while(op!=5):
            print("Digite 1 para Insertar un valor")
            print("Digite 2 para Eliminar un valor")
            print("Digite 3 para Buscar un valor")
            print("Digite 4 para mostrar la tabla")
            print("Digite 5 para REGRESAR al menu princilap")
            op = c.correcion()
            if(op == 1):
                print("Digite el valor a insertar")
                x = input()
                f.Insertar(x)
                print("Insertado!")
            elif(op==2):
                print("Digite el valor a eliminar")
                x = input()
                f.Eliminar(x)
            elif(op==3):
                print("Digite el valor a buscar")
                x = input()
                f.Buscar(x)
            elif(op==5):
                print("Regresando al MENU")
            elif(op==4):
                f.mostrar()
                print("")
    elif(opcion==3):
        print("Gracias por usar el programa")
            